#############################################################################################################################
# Function to execute the actual REST call to a proxy endpoint
# Input:
#   proxyUrl: the actual URL to the proxy service (given by the VLP)
#   requestBody: body following the syntax of the VLP to perform a sparql query on the proxy endpoint
# Output:
#   String containing HTML response after request
#############################################################################################################################
performRestCall <- function(proxyUrl, requestBody, verbose=FALSE, genericContentType=NA, update=FALSE, genericAccept=NA) {
  #create output reader
  h = basicTextGatherer()
  
  contentType = "application/sparql-query"
  acceptType <-  "application/sparql-results+json"
  
  if(update) {
    contentType = "application/x-www-form-urlencoded"
  }
  
  if(!is.na(genericContentType)) {
    contentType = genericContentType
  }
  
  if(!is.na(genericAccept)) {
    acceptType = genericAccept
  }
  
  headerEnvelop = c(Accept=acceptType, 'Content-Type' = contentType)
  if(update) {
    headerEnvelop = c('Content-Type' = contentType)
  }
  
  #reset output reader
  h$reset()
  
  #perform HTTP post, and redirect output to output reader
  curlPerform(url = proxyUrl,
              httpheader=headerEnvelop,
              postfields=requestBody,
              writefunction = h$update,
              verbose = verbose
  )
  
  #read output reader
  responseBody = h$value()
  
  #reset/close HTTP connection
  h$reset()
  
  #return response body
  responseBody
}

#############################################################################################################################
# Function to parse the response body from JSON to an R data.frame object
# Input:
#   responseBody: HTML body from response of REST call
# Output:
#   data.frame containing the parsed result of the HTML response body
#############################################################################################################################
parseResponseBody <- function(responseBody) {
  #remove newline form JSON result string
  jsonParsed <- fromJSON(gsub("\r\n", "", responseBody))
  
  #get result from JSON object, and parse again for actual list of variables and results
  if("SubmitSparqlQueryResult" %in% names(jsonParsed)) {
    jsonParsed <- fromJSON(jsonParsed$SubmitSparqlQueryResult)
  }
  
  jsonVars <- (jsonParsed$head)$vars
  jsonResults <- (jsonParsed$results)$bindings
  
  #loop over rows
  for(var in jsonVars) {
    if(sum(var %in% colnames(jsonResults))>0) {
      internalFrame <- jsonResults[,var]
      
      jsonResults[,var] <- internalFrame$value
      
      if(length(unique(internalFrame$type[!is.na(internalFrame$type)]))==1) {
        varType <- unique(internalFrame$type[!is.na(internalFrame$type)])
        dataType <- unique(internalFrame$datatype)
        
        varTypeDetected <- FALSE;
        
        if(varType=="uri") {
          varTypeDetected <- TRUE;
        } else {
          # if it's a literal, but no type, assume it's null
          if(varType=="literal" && is.null(dataType)) {
            varTypeDetected <- TRUE;
          } else {
            
            dataType <- dataType[!is.na(dataType)]
            
            if(dataType=="http://www.w3.org/2001/XMLSchema#int" || 
               dataType=="http://www.w3.org/2001/XMLSchema#integer" || 
               dataType=="http://www.w3.org/2001/XMLSchema#double") {
              varTypeDetected <- TRUE;
              jsonResults[,var] <- as.numeric(jsonResults[,var])
            }
            
            if(dataType=="http://www.w3.org/2001/XMLSchema#string") {
              varTypeDetected <- TRUE;
            }
            
            if(dataType=="http://www.w3.org/2001/XMLSchema#boolean") {
              varTypeDetected <- TRUE;
              jsonResults[,var] <- tolower(jsonResults[,var])=="true"
            }
            
            if(dataType=="http://www.w3.org/2001/XMLSchema#date") {
              varTypeDetected <- TRUE;
              jsonResults[,var] <- as.Date(jsonResults[,var])
            }
            
            if(dataType=="http://www.w3.org/2001/XMLSchema#dateTime") {
              varTypeDetected <- TRUE;
              jsonResults[,var] <- as.POSIXct(gsub("T|Z", " ", jsonResults[,var]))
            }
            
          }
          
        }
        
        if(!varTypeDetected) {
          warning(paste("Could not detect R class type for RDF data type ", dataType, " for column ", var))
        }
      
      } else {
        warning(paste("No uniform RDF data type for variable ", var))
      }
    } else {
      jsonResults[,var] <- NA
    }
  }
  
  #return final data.frame with result values
  jsonResults
}

#############################################################################################################################
# Function to execute all above functions in the right order. First creates the request body, performs the
#   SPARQL query, and finally parses the body of the response body.
# Input:
#   endpointLocation: the actual URL of the SPARQL endpoint
#   query: actual SPARQL query to perform
# Output:
#   data.frame containing the parsed result of the HTML response body
#############################################################################################################################
performSparqlQuery <- function(endpointLocation, query, verbose=FALSE, genericContentType=NA, update=FALSE) {
  #remove tabs
  query <- gsub("\\t", " ", query)
  
  #replace plus sign
  if(update) { query <- gsub("\\+", "%2B", query) }
  
  #create request body
  requestBody <- query
  
  if(!is.na(genericContentType)) {
    requestBody <- paste0("query=", query)
  }
  
  if(update) {
    requestBody <- paste0("update=", query)
  }
  
  if(verbose) {
    message(requestBody)
  }
  
  #perform REST call on url
  resultBody <- performRestCall(endpointLocation, requestBody, verbose=verbose, genericContentType=genericContentType, update=update)
  
  if(!update) {
    #parse result of query
    results <- parseResponseBody(resultBody)
    #return parsed query result
    results
  } else {
    resultBody
  }
  
  
}

#############################################################################################################################
# Function to execute all above functions in the right order. First creates the request body, performs the
#   SPARQL query, and finally parses the body of the response body.
# Input:
#   endpointLocation: the actual URL of the SPARQL endpoint
#   query: actual SPARQL query to perform
#   token: time-based access token
#   key: key indicating the enpoint to query
# Output:
#   data.frame containing the parsed result of the HTML response body
#############################################################################################################################
performSparqlQuery.vlp <- function(endpointLocation, query, key, token, verbose=FALSE) {
  #remove tabs
  query <- gsub("\\t", " ", query)
  
  #create request body
  requestBody <- toJSON(data.frame(
    token=token,
    endpoint=key,
    query=query
  ), auto_unbox = TRUE)
  #requestBody <- paste0("{ \"token\":\"", token, "\", \"endpoint\":\"", key, "\", \"query\":\"", query ,"\" }", sep="")
  requestBody <- gsub("[", "", requestBody, fixed=TRUE)
  requestBody <- gsub("]", "", requestBody, fixed=TRUE)
  
  if(verbose) {
    message(requestBody)
  }
  
  #perform REST call on url
  resultBody <- performRestCall(endpointLocation, requestBody, verbose=verbose, genericContentType="application/json", genericAccept="application/json")
    
  #parse result of query
  results <- parseResponseBody(resultBody)
  #return parsed query result
  results
  
  
}